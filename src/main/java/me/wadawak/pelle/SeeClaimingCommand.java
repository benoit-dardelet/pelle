package me.wadawak.pelle;

import net.kyori.adventure.text.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static me.wadawak.pelle.Pelle.conn;
import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class SeeClaimingCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(sender instanceof Player player && args.length==0){
            String sqlSeeClaiming="SELECT players.claiming_state FROM players WHERE pseudonym=? AND uuid=?";
            try (PreparedStatement statement = conn.prepareStatement(sqlSeeClaiming)) {
                statement.setString(1, player.getName());
                statement.setString(2, player.getUniqueId().toString());
                ResultSet resultSet = statement.executeQuery();
                TextComponent stateMess;
                switch(resultSet.getInt("claiming_state")){
                    case 0 -> stateMess = text("Your claiming state is currently set to:  off. \n --> You are not contributing to your group's occupation score.").color(color(0xffc717));
                    case 1 -> stateMess = text("Your claiming state is currently set to:  passive. \n --> You are contributing to your group's occupation score only where it is already the conquering group.").color(color(0xffc717));
                    case 2 -> stateMess = text("Your claiming state is currently set to:  aggressive. \n --> You are contributing to your group's occupation score, even at the expense of other groups.").color(color(0xffc717));
                    default -> stateMess = text("The server is facing a problem: please report this message to server admins.").color(color(0xffc717));
                }
                player.sendMessage(stateMess);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }
}
