package me.wadawak.pelle;

import net.kyori.adventure.text.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static me.wadawak.pelle.Pelle.conn;
import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class SetClaimingCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(sender instanceof Player player && args.length==1){
            String sqlSetClaiming;
            String resultMess;
            String arg = args[0].toLowerCase();
            switch (arg) {
                case "off", "o", "0" -> {
                    sqlSetClaiming = "UPDATE players SET claiming_state = 0 WHERE pseudonym = ? AND uuid = ?";
                    resultMess = "off. \n --> From now on, you will not contribute to your group's occupation score.";
                }
                case "passive", "pass", "p", "1" -> {
                    sqlSetClaiming = "UPDATE players SET claiming_state = 1 WHERE pseudonym = ? AND uuid = ?";
                    resultMess = "passive. \n --> From now on, you will contribute to your group's occupation score only where it is already the conquering group.";
                }
                case "aggressive", "agg", "ag", "a", "2" -> {
                    sqlSetClaiming = "UPDATE players SET claiming_state = 2 WHERE pseudonym = ? AND uuid = ?";
                    resultMess = "aggressive. \n --> From now on, you will contribute to your group's occupation score, even at the expense of other groups.";
                }
                default -> {
                    return false;
                }
            }
            // In "players" table, "claiming_state" column, 0 means "off", 1 means "passive" and 2 means "aggressive"
            try (PreparedStatement statement = conn.prepareStatement(sqlSetClaiming)) {
                statement.setString(1, player.getName());
                statement.setString(2, player.getUniqueId().toString());
                statement.executeUpdate();
                final TextComponent OkMess = text("Your claiming state has been successfully set to:  " + resultMess).color(color(0xffc717));
                player.sendMessage(OkMess);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }
}
