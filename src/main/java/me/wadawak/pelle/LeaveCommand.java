package me.wadawak.pelle;

import net.kyori.adventure.text.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static me.wadawak.pelle.Pelle.conn;
import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class LeaveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {

        //Breaking the command logic if sender is console or something was written as arguments.
        if (!(sender instanceof Player player) || args.length != 0) {
            return false;
        }

        // 1- Getting belonging_group_id of player.
        String sql_step_1 = "SELECT * FROM players WHERE uuid=?;";
        int belonging_group_id = -1;
        try (PreparedStatement step_1 = conn.prepareStatement(sql_step_1)) {
            step_1.setString(1, player.getUniqueId().toString());
            ResultSet rs_step_1 = step_1.executeQuery();
            belonging_group_id = rs_step_1.getInt("belonging_group_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 2- Breaking the command logic if belonging_group_id is null.
        if (belonging_group_id == 0) {
            TextComponent alreadyGroupFreeMess = text("You cannot leave any group because you are already not a member of any group.").color(color(0xffc717));
            player.sendMessage(alreadyGroupFreeMess);
            return false;
        }

        // 3- Setting player's belonging_group_id to null.
        String sql_step_3 = "UPDATE players SET belonging_group_id=null WHERE uuid=?";
        try (PreparedStatement step_3 = conn.prepareStatement(sql_step_3)) {
            step_3.setString(1, player.getUniqueId().toString());
            step_3.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 4- Ending the command logic ...
        String sql_step_4 = "SELECT * FROM players WHERE belonging_group_id=?";
        try (PreparedStatement step_4 = conn.prepareStatement(sql_step_4)) {
            step_4.setInt(1, belonging_group_id);
            ResultSet rs_step_4 = step_4.executeQuery();
            // ... only if there are still members in the just-left group.
            if (rs_step_4.next()){
                TextComponent successMess = text("You have successfully left your group. From now on, you are free to join or create any group.").color(color(0xffc717));
                player.sendMessage(successMess);
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // 5- Deleting the just-left group because there is no member left in the just-left group.
        String sql_step_5 = "DELETE FROM groups WHERE group_id=?";
        try (PreparedStatement step_5 = conn.prepareStatement(sql_step_5)) {
            step_5.setInt(1, belonging_group_id);
            step_5.executeUpdate();
            TextComponent successDeleteMess = text("You have successfully left your group and as it has become empty, it has been deleted. From now on, you are free to join or create any group.").color(color(0xffc717));
            player.sendMessage(successDeleteMess);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}