package me.wadawak.pelle;

import net.kyori.adventure.text.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static me.wadawak.pelle.Pelle.conn;
import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class SetGroupNameCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        //Breaking the command logic if sender is console or nothing was written as arguments.
        if (!(sender instanceof Player player) || args.length == 0) {
            return false;
        }

        // 1- Getting belonging_group_id of player.
        String sql_step_1 = "SELECT belonging_group_id FROM players WHERE uuid=?";
        int belonging_group_id = -1;
        try (PreparedStatement step_1 = conn.prepareStatement(sql_step_1)) {
            step_1.setString(1, player.getUniqueId().toString());
            ResultSet rs_step_1 = step_1.executeQuery();
            belonging_group_id = rs_step_1.getInt("belonging_group_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Breaking the command logic if player is not a member of any group.
        if (belonging_group_id == 0) {
            TextComponent groupFreeMess = text("You cannot change the name of your group because you are currently not a member of any group.").color(color(0xffc717));
            player.sendMessage(groupFreeMess);
            return false;
        }

        // 2- Preparing groupName.
        StringBuilder recombinedArgs = new StringBuilder();
        for (String part:args){
            recombinedArgs.append(part).append(" ");
        }
        String groupName = recombinedArgs.toString();

        //Breaking the command logic if the wanted group name is too long.
        if (groupName.length() > 100) {
            //Warning player that the group name is too long.
            TextComponent TooLongNameMess = text("You cannot set this name for your group because it is too long (over 100 characters).").color(color(0xffc717));
            player.sendMessage(TooLongNameMess);
            return false;
        }

        // 3- Checking that the wanted group name is not already used.
        String sql_step_3 = "SELECT * FROM groups WHERE group_name=?;";
        try (PreparedStatement step_3 = conn.prepareStatement(sql_step_3)) {
            step_3.setString(1, groupName);
            ResultSet rs_step_3 = step_3.executeQuery();
            if (rs_step_3.next()) {
                //Warning player that the group name is already used by an existing group.
                TextComponent InvalidNameMess = text("You cannot set this name for your group because it is already used by an existing group.").color(color(0xffc717));
                player.sendMessage(InvalidNameMess);
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 4- Setting the new group_name.
        String sql_step_4 ="UPDATE groups SET group_name=? WHERE group_id=?";
        try (PreparedStatement step_4 = conn.prepareStatement(sql_step_4)) {
            step_4.setString(1, groupName);
            step_4.setInt(2, belonging_group_id);
            step_4.executeUpdate();
            TextComponent successMess = text("You have successfully set your group name to: "+groupName+".").color(color(0xffc717));
            player.sendMessage(successMess);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
