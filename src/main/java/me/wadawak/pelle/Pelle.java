package me.wadawak.pelle;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public final class Pelle extends JavaPlugin {

    static ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();

    static Connection conn = null;

    @Override
    public void onEnable() {
        // -------------- ENVOI DU MESSAGE D'ALLUMAGE A LA CONSOLE
        console.sendMessage("Démarrage du plugin Pelle");

        // -------------- ENREGISTREMENT DES EVENT LISTENERS
        getServer().getPluginManager().registerEvents(new MoveEventListener(), this);
        getServer().getPluginManager().registerEvents(new JoinEventListener(), this);

        // -------------- ENREGISTREMENT DES COMMANDES
        getCommand("hello").setExecutor(new HelloCommand());
        getCommand("alert").setExecutor(new AlertCommand());
        getCommand("setClaiming").setExecutor(new SetClaimingCommand());
        getCommand("seeClaiming").setExecutor(new SeeClaimingCommand());
        getCommand("createGroup").setExecutor(new CreateGroupCommand());
        getCommand("leave").setExecutor(new LeaveCommand());
        getCommand("seeGroupName").setExecutor(new SeeGroupNameCommand());
        getCommand("setGroupName").setExecutor(new SetGroupNameCommand());

        // -------------- CONNEXION A LA BASE DE DONNEES SQLITE + CREATION SI ELLE N'EXISTE PAS DEJA

        File directory = new File("plugins/pelle");
        if (!directory.exists()) {
            directory.mkdirs(); // Creates parent directories as needed
        }

        try {
            // db parameters
            String url = "jdbc:sqlite:plugins/pelle/chunk.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        String sql_groups_creation = "CREATE TABLE IF NOT EXISTS groups (\n"
                +"group_id integer PRIMARY KEY,\n"
                +"group_name text NOT NULL\n"
                +");";

        String sql_players_creation = "CREATE TABLE IF NOT EXISTS players (\n"
                + "	player_id integer PRIMARY KEY,\n"
                + "	pseudonym text NOT NULL,\n"
                + " uuid text NOT NULL, \n"
                + " claiming_state integer NOT NULL DEFAULT 0, \n"
                + " belonging_group_id integer, \n"
                + " FOREIGN KEY(belonging_group_id) REFERENCES groups(group_id) \n"
                + ");";

        try (
                Statement stmt = conn.createStatement()
        ) {
            // create a new table
            stmt.execute(sql_groups_creation);
            stmt.execute(sql_players_creation);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void onDisable() {
        try {
            if (conn != null) {
                conn.close();
                System.out.println("Connection to SQLite has been stopped.");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
