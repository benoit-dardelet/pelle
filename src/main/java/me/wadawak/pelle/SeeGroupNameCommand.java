package me.wadawak.pelle;

import net.kyori.adventure.text.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static me.wadawak.pelle.Pelle.conn;
import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class SeeGroupNameCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {

        //Breaking the command logic if sender is console or something was written as arguments.
        if (!(sender instanceof Player player) || args.length != 0) {
            return false;
        }

        // 1- Getting belonging_group_id of player.
        String sql_step_1 = "SELECT belonging_group_id FROM players WHERE uuid=?";
        int belonging_group_id = -1;
        try (PreparedStatement step_1 = conn.prepareStatement(sql_step_1)) {
            step_1.setString(1, player.getUniqueId().toString());
            ResultSet rs_step_1 = step_1.executeQuery();
            belonging_group_id = rs_step_1.getInt("belonging_group_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 2- If it is null, telling player that he is not a member of any group and ending the command logic.
        if (belonging_group_id == 0) {
            TextComponent groupFreeMess = text("You are currently not a member of any group.").color(color(0xffc717));
            player.sendMessage(groupFreeMess);
            return false;
        }

        // 3- Getting group_name of that group_id, telling player the current name of his group and ending the command logic.
        String sql_step_3 = "SELECT group_name FROM groups WHERE group_id=?";
        String groupName = "";
        try (PreparedStatement step_3 = conn.prepareStatement(sql_step_3)) {
            step_3.setInt(1, belonging_group_id);
            ResultSet rs_step_3 = step_3.executeQuery();
            groupName = rs_step_3.getString("group_name");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        TextComponent groupNameMess = text("You are currently a member of a group whose name is: "+groupName+".").color(color(0xffc717));
        player.sendMessage(groupNameMess);
        return true;
    }
}
