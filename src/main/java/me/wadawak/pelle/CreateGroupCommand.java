package me.wadawak.pelle;


import net.kyori.adventure.text.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static me.wadawak.pelle.Pelle.conn;
import static me.wadawak.pelle.Pelle.console;
import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class CreateGroupCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {

        // --- SERIES OF CHECKS ON PLAYER'S SIDE ---

        //Breaking the command logic if sender is console or no group name was passed as arguments.
        if (!(sender instanceof Player player) || args.length == 0) {
            return false;
        }
        //Breaking the command logic if the group name is too long.
        StringBuilder recombinedArgs = new StringBuilder();
        for (String part:args){
            recombinedArgs.append(part).append(" ");
        }
        String groupName = recombinedArgs.toString();
        if (groupName.length() > 100) {
            //Warning player that the group name is too long.
            TextComponent TooLongNameMess = text("You cannot create a group with this name because it is too long (over 100 characters).").color(color(0xffc717));
            player.sendMessage(TooLongNameMess);
            return false;
        }
        //Breaking the command logic if player is already a member of a group, which forbids him/her to create a new one.
        String sql_memberOfAGroup = "SELECT belonging_group_id FROM players WHERE uuid=?;";
        try (PreparedStatement statement = conn.prepareStatement(sql_memberOfAGroup)) {
            statement.setString(1, player.getUniqueId().toString());
            ResultSet rs = statement.executeQuery();
            console.sendMessage(Integer.toString(rs.getInt("belonging_group_id")));
            if (rs.getInt("belonging_group_id") != 0) {
                //Warning player that he is already a member of a group.
                TextComponent AlreadyInAGroupMess = text("You cannot create a group because you are currently a member of an existing group.").color(color(0xffc717));
                player.sendMessage(AlreadyInAGroupMess);
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // --- CHECKS AND UPDATES ON GROUPS' SIDE ---

        // 1 - Checking that the group name is not already used.
        String sql_step_1 = "SELECT * FROM groups WHERE group_name=?;";
        try (PreparedStatement step_1 = conn.prepareStatement(sql_step_1)) {
            step_1.setString(1, groupName);
            ResultSet rs_step_1 = step_1.executeQuery();
            if (rs_step_1.next()) {
                // Warning player that the group name is already used by an existing group.
                TextComponent InvalidNameMess = text("You cannot create a group with this name because it is already used by an existing group.").color(color(0xffc717));
                player.sendMessage(InvalidNameMess);
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 2 - Adding the new group to the groups table.
        String sql_step_2 = "INSERT INTO groups (group_name) VALUES (?);";
        try (PreparedStatement step_2 = conn.prepareStatement(sql_step_2)) {
            step_2.setString(1, groupName);
            step_2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 3 - Retrieving group_id of the group we've just created (as group_id is needed for step 4).
        int groupJustCreated_id = 0;
        String sql_step_3 = "SELECT * FROM groups WHERE group_name=?;";
        try (PreparedStatement step_3 = conn.prepareStatement(sql_step_3)) {
            step_3.setString(1, groupName);
            ResultSet rs_step_3 = step_3.executeQuery();
            groupJustCreated_id = rs_step_3.getInt("group_id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // 4 - Finally, making player a member of the group he/she has just created.
        String sql_step_4 = "UPDATE players SET belonging_group_id=? WHERE uuid=?;";
        try (PreparedStatement step_4 = conn.prepareStatement(sql_step_4)) {
            step_4.setInt(1, groupJustCreated_id);
            step_4.setString(2, player.getUniqueId().toString());
            step_4.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Informing player that he has just created a group and is now member of this group.
        TextComponent SuccessMess = text("Well done! You have successfully created a group named: "+groupName+". From now on, you are a member of this group.").color(color(0xffc717));
        player.sendMessage(SuccessMess);
        return true;
    }
}

