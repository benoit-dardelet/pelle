package me.wadawak.pelle;


import net.kyori.adventure.text.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import static net.kyori.adventure.text.Component.text;
import static net.kyori.adventure.text.format.TextColor.color;

public class AlertCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {

        if (!(sender instanceof Player)){ return false;}
        if (args.length==0){ return false;}
        Player player = (Player) sender;
        final TextComponent prefix = text("["+player.getName()+"] ").color(color(0xffc717));
        StringBuilder message = new StringBuilder();
        for (String part : args){
            message.append(part).append(" ");
        }
        final TextComponent bc = prefix.append(text(message.toString()).color(color(0xffffff)));
        Bukkit.broadcast(bc);
        return true;
    }
}